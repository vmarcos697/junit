package marcos.junit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    @Test
    public void nullTest(){
        Person person = new Person("Bryan", "Torres");
        assertNull(person, "Error, the object contains data");

    }

    @Test
    public void notNullTest(){
         Person person = new Person("Nancy", "Petersen");
         assertNotNull(person);

    }

    @Test
    public void notSameTest(){
        Person personOne = new Person("John", "Solis");
        Person personTwo = new Person("John", "Solis");
        assertNotSame(personOne, personTwo);

    }

    @Test
    public void sameTest(){
        Person personOne = new Person("John", "Solis");
        Person personTwo = personOne;
        assertSame(personOne, personTwo);

    }


}