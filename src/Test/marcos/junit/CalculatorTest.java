package marcos.junit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    private Calculator calc;

    @Test
    public void testAdd(){
        Calculator calc = new Calculator();
        assertTrue(calc.add(2, 2)==(2+2));
        assertFalse(calc.add(3, 5)==(7));
    }

    @Test
    public void testSubs(){
        Calculator calc = new Calculator();
        assertEquals(4, calc.subs(5, 2) , "The result is 3");
    }

    @Test
    public void testNewArray(){
        Calculator calc = new Calculator();
        int expected[] = new int[]{4,8,2};
        assertArrayEquals(expected, calc.NewArray(new int[]{3,7,1}));
    }

    @Test
    public void test(){
        Calculator calc = new Calculator();
        assertNotEquals(4, 9);
    }

}