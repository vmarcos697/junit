package marcos.junit;

public class Calculator {

    private int answer;

    public Calculator(){
        answer = 0;
    }

    public int add(int a, int b){
        answer = a + b;
        return answer;
    }

    public int subs(int a, int b){
        answer = a - b;
        return answer;
    }

    public int add(int v){
        answer += v;
        return answer;
    }

    public int subs(int v){
        answer -= v;
        return answer;
    }

    public int answer(){
        return answer;
    }

    public int[] NewArray (int[] numbers){
        int length = numbers.length;
        int output[] = new int[length];
        for(int i=0; i<length; i++){
            output[i] = numbers[i]+1;
        }
        return output;
    }

}
